package com.and.codingtest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication

public class TelecomApplication  extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(TelecomApplication.class);

	public static void main(String[] args) {
		logger.info("Service is started...");

		SpringApplication.run(TelecomApplication.class, args);
	}

	

	
}
