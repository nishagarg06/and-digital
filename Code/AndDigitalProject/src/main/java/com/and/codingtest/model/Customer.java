package com.and.codingtest.model;


import java.util.ArrayList;


public class Customer {
	
	String customerId = null;
	String customerName = null;
	
	ArrayList<PhoneNumber> phoneNumbers = null;
	
	
	public ArrayList<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(ArrayList<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getcustomerId() {
		return customerId;
	}
	public void setcustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
