package com.and.codingtest.model;

public class PhoneNumber {
	
	String customerId = null;
	String phoneNumber = null;
	boolean activeStatus = false;
	
	public String getcustomerId() {
		return customerId;
	}
	public void setcustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}
	
	

}
