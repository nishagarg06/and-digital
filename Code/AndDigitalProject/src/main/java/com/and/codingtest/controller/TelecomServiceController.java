package com.and.codingtest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.and.codingtest.exception.CustomerNotfoundException;
import com.and.codingtest.exception.PhoneNumberNotfoundException;
import com.and.codingtest.model.Customer;
import com.and.codingtest.model.PhoneNumber;


@RestController
public class TelecomServiceController {
   private static Map<String, Customer> customerRepo = new HashMap<>();
   private static Map<String, PhoneNumber> phoneNumberRepo = new HashMap<>();
   static ArrayList<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
   static PhoneNumber phoneNum1;
   static PhoneNumber phoneNum2;
   static PhoneNumber phoneNum3;
   static{
	   phoneNum1 = new PhoneNumber();
	   phoneNum1.setcustomerId("C0001");
	   phoneNum1.setPhoneNumber("9876543210");
	   phoneNum1.setActiveStatus(true);
	   phoneNumberRepo.put(phoneNum1.getPhoneNumber(),phoneNum1);
	   
	   phoneNum2 = new PhoneNumber();
	   phoneNum2.setcustomerId("C0001");
	   phoneNum2.setActiveStatus(false);
	   phoneNum2.setPhoneNumber("8765432109");
	   phoneNumberRepo.put(phoneNum2.getPhoneNumber(),phoneNum2);
	   
	   phoneNum3 = new PhoneNumber();
	   phoneNum3.setcustomerId("C0002");
	   phoneNum3.setActiveStatus(false);
	   phoneNum3.setPhoneNumber("7654321089");
	   phoneNumberRepo.put(phoneNum3.getPhoneNumber(),phoneNum3);
	   
   }
   

   static {
      Customer customer1 = new Customer();
      customer1.setcustomerId("C0001");
      customer1.setCustomerName("Customer 1");
      phoneNumbers.add(phoneNum1);
      phoneNumbers.add(phoneNum2);
      customer1.setPhoneNumbers(phoneNumbers);
      customerRepo.put(customer1.getcustomerId(), customer1);
      
      phoneNumbers = new ArrayList<PhoneNumber>();
      Customer customer2 = new Customer();
      customer2.setcustomerId("C0002");
      customer2.setCustomerName("Customer 2");
      phoneNumbers.add(phoneNum3);
      customer2.setPhoneNumbers(phoneNumbers);
      customerRepo.put(customer2.getcustomerId(), customer2);
   }
   
   @RequestMapping(value = "/")
	public String hello() {
		return "Hello, Welcome to Telecom Service !!!";
	}
   
   @RequestMapping(value = "/allphonenumbers")
   public ResponseEntity<Object> getAllNumbers() {
      return new ResponseEntity<>(phoneNumberRepo.values(), HttpStatus.OK);
   }
   
   @RequestMapping(value = "/customer/{id}")
   public ResponseEntity<Object> getNumberById(@PathVariable("id") String id) {
	   if(!customerRepo.containsKey(id))throw new CustomerNotfoundException();
      return new ResponseEntity<>(customerRepo.get(id), HttpStatus.OK);
   }
   
   @RequestMapping(value = "/activateNumber/{number}")
   public ResponseEntity<Object> activatePhoneNumber(@PathVariable("number") String number) { 
	   if(!phoneNumberRepo.containsKey(number))throw new PhoneNumberNotfoundException();
      PhoneNumber phoneNumber = phoneNumberRepo.get(number);
	  phoneNumberRepo.remove(number);
	  
	  
	  Customer customer = customerRepo.get(phoneNumber.getcustomerId());
	  customerRepo.remove(phoneNumber.getcustomerId());
	  
	  ArrayList<PhoneNumber> phoneNumbers = customer.getPhoneNumbers();
	  
	  for (int i=0; i<phoneNumbers.size() ; i++) 
	  {
		  PhoneNumber num = phoneNumbers.get(i);
		  if(num.equals(phoneNumber)) {
			  num.setActiveStatus(true);
			  phoneNumbers.set(i, num);
			  break;
		  }
	  }
	  
	  phoneNumber.setActiveStatus(true);
	  phoneNumberRepo.put(number, phoneNumber);
	  
	  customer.setPhoneNumbers(phoneNumbers);
	  customerRepo.put(phoneNumber.getcustomerId(), customer);
	  
	  return new ResponseEntity<>("Phone Number:"+number+" is activated successsfully", HttpStatus.OK);
   }   
   
   
   
}