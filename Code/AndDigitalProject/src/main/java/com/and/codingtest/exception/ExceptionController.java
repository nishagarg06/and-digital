package com.and.codingtest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
	 @ExceptionHandler(value = CustomerNotfoundException.class)
	   public ResponseEntity<Object> exception(CustomerNotfoundException exception) {
	      return new ResponseEntity<>("Customer not found in database.", HttpStatus.NOT_FOUND);
	   }
	 
	 @ExceptionHandler(value = PhoneNumberNotfoundException.class)
	   public ResponseEntity<Object> exception(PhoneNumberNotfoundException exception) {
	      return new ResponseEntity<>("Phone Number not found in database.", HttpStatus.NOT_FOUND);
	   }
}
